<?php

/**
 * @file
 * Exposes Rules action components as content types.
 */

/**
 * Plugin definition.
 */
$plugin = array(
  'title' => t('Rules Action Trigger'),
  'content type' => 'rules_trigger_trigger_content_type_content_type',
  'defaults' => array(
    'label' => '',
  ),
);

/**
 * Returns a single subtype.
 */
function rules_trigger_trigger_content_type_content_type($subtype) {
  $types = rules_trigger_trigger_content_type_content_types();
  if (isset($types[$subtype])) {
    return $types[$subtype];
  }
}

/**
 * Return all available subtypes.
 */
function rules_trigger_trigger_content_type_content_types() {
  $types = &drupal_static(__FUNCTION__);
  if (isset($types)) {
    return $types;
  }

  $actions = rules_get_components(FALSE, 'action');
  $category = t('Rules Action Triggers');
  foreach ($actions as $name => $action) {
    $description = !empty($action->tags) ? t('Tags: %tags', array('%tags' => implode(',', $action->tags))) : '';

    $contexts = array();
    foreach ($action->componentVariables() as $key => $variable) {
      $contexts[$key] = rules_trigger_ctools_get_argument_context($variable);
    }

    $types[$name] = array(
      'category' => $category,
      'title' => entity_label('rules_config', $action),
      'description' => $description,
      'required context' => array_values($contexts),
      'context map' => array_keys($contexts),
    );
  }

  return $types;
}

/**
 * Renders the subtype.
 */
function rules_trigger_trigger_content_type_render($subtype, $config, $panels, $context) {
  if (!empty($context)) {
    foreach ($context as $item) {
      if (!isset($item->data) && $item->identifier != 'No context') {
        // Bail out if any of the provided contexts is not set.
        return FALSE;
      }
    }
  }

  if (!$component = rules_config_load($subtype)) {
    return FALSE;
  }

  $arguments = array();
  if (!empty($context)) {
    foreach ($context as $key => $item) {
      // Map each given context to the rules variables it belong to.
      $arguments[$config['context map'][$key]] = $item->data;
    }
  }

  $content = drupal_get_form('rules_trigger_form', $component, $arguments, $config['label']);

  // Build the content type block.
  $block = new stdClass();
  $block->module = 'rules_trigger';
  $block->title = entity_label('rules_config', $component);
  $block->content = drupal_render($content);
  $block->delta = $subtype;

  return $block;
}

/**
 * Settings form callback.
 */
function rules_trigger_trigger_content_type_edit_form($form, &$form_state) {
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#default_value' => $form_state['conf']['label'],
  );

  return $form;
}

/**
 * Settings form submit callback.
 */
function rules_trigger_trigger_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf']['context map'] = $form_state['subtype']['context map'];
  $form_state['conf']['label'] = $form_state['values']['label'];
}

/**
 * Returns the administrative title for a type.
 */
function rules_trigger_trigger_content_type_admin_title($subtype, $conf, $context) {
  if ($component = rules_config_load($subtype)) {
    return t('Rules Action Trigger: @name', array('@name' => entity_label('rules_config', $component)));
  }
}

/**
 * Helper function to create CTools contexts from Rules variables.
 *
 * @todo Check whether we can use the Rules Data Selector for all of this by
 * mapping the entire tree of available contexts from CTools.
 */
function rules_trigger_ctools_get_argument_context($variable) {
  // Declare variables as optional contexts to allow for user provided
  // context input in the front-end.
  if ($variable['type'] == 'entity') {
    return new ctools_context_optional($variable['label'], 'entity');
  }
  else if ($entity = entity_get_info($variable['type'])) {
    return new ctools_context_optional($variable['label'], "entity:{$variable['type']}");
  }

  // @todo Replace this with simple input form fields with token support.
  return new ctools_context_optional($variable['label'], 'any');
}
